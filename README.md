# README #

User Creation and Login Process

### Install Dependencies ####
go get github.com/go-sql-driver/mysql

go get github.com/gorilla/securecookie

### What is this repository for? ###
A user is able to come to a login/signup page that allows them to log-in or if they are not an existing user, sign up as a new user. User can be able to use their linkedin account to login. In either case, the user will have a basic profile information page after signing up or logging in which will also be editable. Also, if the user has forgotten password, they will be able to get a reset password email link and by following it, be able to reset the password


### How do I get set up? ###

* nginx.conf - Reverse proxy server configuration. Added default filserver and apiserver port. I used ubuntu for the config, modify /etc/nginx/sites-available/default and service restart
* fileserver - Serve all static file, default port: 4000
* apiserver - Serve API calls, default port: 6000
* Database configuration: mysql - default user: root passord: root and database: social
* All dependent javascript/css file directly called.