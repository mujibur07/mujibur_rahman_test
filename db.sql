CREATE TABLE users  (
        `id` INT(10)  UNSIGNED    NOT NULL    AUTO_INCREMENT,
        `fullname`    VARCHAR(32),
        `address` TEXT,
        `email`   VARCHAR(64) NOT NULL,
        `password` VARCHAR(64) NOT NULL,
        `telephone`   VARCHAR(32),
        `registertime`  TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE  INDEX   `email`   (`email`)
)